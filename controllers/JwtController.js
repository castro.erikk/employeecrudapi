// JsonWebToken signing and verification middleware
const jwt = require('jsonwebtoken')

// Supplies the JSON TOKEN from the payload. Assumption that user is present in the database
const supplyToken = (req, res) => {
    // Sign JWT with the payload and the secret key
    const token = jwt.sign({username: req.body.username}, process.env.SECRET_KEY)
    return res.status(200).send(token)
}

const verifyToken = (req, res, next) => {
    if (!req.get('Authorization')) {
        return res.status(400).json({success: false, message: 'Authorization Required'})
    }
    // Check if Bearer token is sent
    if(req.get('Authorization').substring(0,7) !== 'Bearer ') {
        return res.status(400).json({success: false, message: 'Invalid Authorization Header'})
    }
    const token = req.get('Authorization').split(' ')[1]
    try {
        const payload = jwt.verify(token, process.env.SECRET_KEY)
        res.locals.payload = payload
        next()
    } catch(error) {
        return res.status(400).json({success: false, message: 'Invalid or Expired token'})
    }
}

module.exports = {
    supplyToken,
    verifyToken
}