const express = require('express')
const mongoose = require('mongoose')
const app = express()
require('dotenv').config()
const port = process.env.PORT
const cors = require ('cors')

// Middlewares
app.use(cors());
app.options('*', cors());
app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.get('/', (req, res) => {
    res.status(200).send('Hello World!')
})

mongoose.connect(process.env.DB_MONGODB, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true
})

mongoose.connection.on('error', console.error.bind(console, 'connection error:'))
mongoose.connection.once('open', () => console.log('Connected to MongoDB Atlas'))

/* 
    Regarding this code, we are accessing the router exported from the employee.js in the routes folder,
    we are also using the route middleware on a certain route like /api
    For example, getting the get request stated in the employee.js in the routes folder
        the link would be /api/employees since we placed the middleware in the /api route
*/
const employeeRouter = require('./routes/Employee')
const jwtRouter = require('./routes/Jwt')
const userRouter = require('./routes/User')

const { verifyToken } = require('./controllers/JwtController')

app.use('/api/employees', verifyToken)
app.use('/api', [employeeRouter, jwtRouter, userRouter])

app.listen(port, () => console.log(`listening on port ${port}`))